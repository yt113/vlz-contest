import json
import hashlib
import random
import re

'''
Example:
{
    "11": "https://www.youtube.com/watch?v=YaGqOPxHFkc&feature=youtu.be",
    "12": "https://www.youtube.com/watch?v=YaGqOPxHFkc",
    "13": "https://youtu.be/YaGqOPxHFkc",
    "14": "https://www.youtube.com/embed/YaGqOPxHFkc?rel=0"
}
'''
def filter_url(url):
    p = re.compile("(https?:\/\/)?(www\.)?(youtu\.be\/|youtube\.com(\/watch\?v=|\/embed\/))([^&\n?]*).*")
    r = p.match(url)
    key = r.groups()[4]
    return "https://www.youtube.com/embed/" + key + "?rel=0"


def get_playlist():
    entry_list = json.load(open('team_entries.json', 'r'))
    res = []
    for entry in entry_list.items():
        team_num, url = entry
        url = filter_url(url)
        encoded_team_num = hashlib.md5(team_num).hexdigest()
        res.append((encoded_team_num[:6], team_num, url))
    random.shuffle(res)
    return res

def jsonfy(pl):
    # re shuffle
    pl.sort(key=lambda x:int(x[1]))
    with open('mapped_playlist.json', 'w') as f:
        json.dump(pl, f, indent=4)

if __name__ == '__main__':
    pl = get_playlist()
    print pl
    jsonfy(pl)
