vlz-contest
===
This is a static website version. The main logic has been moved to the static/assets/js/main.js (I added codes from line #319 to #379). It basically read the file `mapped_playlist.json`, re-shuffle the playlist order, and inject into the html codes before the browser start rendering the page. `mapped_playlist.json` is a file generated from `team_entries.json`, with team numbers hashed. If you change `team_entries.json`, you need to generate `mapped_playlist.json` again. There is a script `util.py` to do this.

Once you've done with the data, you don't need to launch the website by yourself, simply put this under some folder is fine. Static server engine like Nginx can take care of this.

Data
---
The original playerlist is stored in `team_entries.json`. Please change the content with real urls.

Here is an example. The key is the team number, the value is the url. The parser accecpt these four  types of url. You can extend the parser to accept more (in util.py).
  ```
  {
      "11": "https://www.youtube.com/watch?v=YaGqOPxHFkc&feature=youtu.be",
      "12": "https://www.youtube.com/watch?v=YaGqOPxHFkc",
      "13": "https://youtu.be/YaGqOPxHFkc",
      "14": "https://www.youtube.com/embed/YaGqOPxHFkc?rel=0"
  }
  ```
  
**IMPORTANT** After that, run
```
$ python util.py
```
once to get `mapped_playlist.json`. This is the data file that `static/assets/js/main.js` will load.

The difference between `team_entries.json` and `mapped_playlist.json` is that the second one includes the md5-mapped ids and the transformed urls (from "...watch?v=..." to "...embed...").
